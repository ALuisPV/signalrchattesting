﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace SignalRChat
{
    public class ChatHub : Hub
    {
        // Este metodo se llama desde LOS CLIENTES
        // Via JQuery se obtiene una REFERENCIA EN LOS CLIENTES a esta clase ChatHub
        // Asi, cuando un cliente envia un mensaje de Chat, ejecuta este Metodo
        // Gracias a SignalR, cada vez que se llama a Send, se llama al metodo "broadcastMessage" de todos los clientes
        // En los clientes, se define el método
        public void Send(string name, string message)
        {
            // Llamar al metodo broadcastMessage para actualizar clientes.
            Clients.All.broadcastMessage(name, message);
        }
    }
}